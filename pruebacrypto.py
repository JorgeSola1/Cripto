from unicodedata import normalize

listaLetras = ('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','w','x','y','z')
listaNumeros = ('0','1','2','3','4','5','6','7','8','9')
listaSimbolos = ('¿','?','=','(',')','.',':',';','!','¡','-','+','{','}',',')
newCodigo = []


def cripto(miTexto, n):
    
    trans_tab = dict.fromkeys(map(ord, u'\u0301\u0308'), None)
    miTexto = normalize('NFKC', normalize('NFKD', miTexto).translate(trans_tab))
    miTexto = miTexto.lower()
    
    for i in miTexto:
        
        if i in listaSimbolos:
            pass
        
        elif i == ' ':
            newCodigo.append(i)
            
        
        elif i in listaLetras[0:len(listaLetras)-n]: 
            newValor = listaLetras[listaLetras.index(i)+n]
            newCodigo.append(newValor)
        
        elif i in listaNumeros[0:len(listaNumeros)-n]:
            newValor = listaNumeros[listaNumeros.index(i)+n]
            newCodigo.append(newValor)
        
        elif i in listaLetras[len(listaLetras)-n:25]:
            
            newValor = listaLetras[listaLetras.index(i)-len(listaLetras)+n]
            newCodigo.append(newValor)
        
        else:    
            newValor = listaNumeros[listaNumeros.index(i) - len(listaNumeros) + n]
            newCodigo.append(newValor)
            
        
    print(miTexto)
    
    for i in newCodigo:
        print(i, end = '')
    
    